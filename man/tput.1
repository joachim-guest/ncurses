'\" t
.\"***************************************************************************
.\" Copyright 2018-2022,2023 Thomas E. Dickey                                *
.\" Copyright 1998-2016,2017 Free Software Foundation, Inc.                  *
.\"                                                                          *
.\" Permission is hereby granted, free of charge, to any person obtaining a  *
.\" copy of this software and associated documentation files (the            *
.\" "Software"), to deal in the Software without restriction, including      *
.\" without limitation the rights to use, copy, modify, merge, publish,      *
.\" distribute, distribute with modifications, sublicense, and/or sell       *
.\" copies of the Software, and to permit persons to whom the Software is    *
.\" furnished to do so, subject to the following conditions:                 *
.\"                                                                          *
.\" The above copyright notice and this permission notice shall be included  *
.\" in all copies or substantial portions of the Software.                   *
.\"                                                                          *
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS  *
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF               *
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   *
.\" IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   *
.\" DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR    *
.\" OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR    *
.\" THE USE OR OTHER DEALINGS IN THE SOFTWARE.                               *
.\"                                                                          *
.\" Except as contained in this notice, the name(s) of the above copyright   *
.\" holders shall not be used in advertising or otherwise to promote the     *
.\" sale, use or other dealings in this Software without prior written       *
.\" authorization.                                                           *
.\"***************************************************************************
.\"
.\" $Id: tput.1,v 1.97 2023/12/31 00:16:41 tom Exp $
.TH @TPUT@ 1 2023-12-30 "ncurses 6.4" "User commands"
.ie \n(.g \{\
.ds `` \(lq
.ds '' \(rq
.\}
.el \{\
.ie t .ds `` ``
.el   .ds `` ""
.ie t .ds '' ''
.el   .ds '' ""
.\}
.
.de bP
.ie n  .IP \(bu 4
.el    .IP \(bu 2
..
.ds d @TERMINFO@
.SH NAME
\fB\%@TPUT@\fP,
\fB\%reset\fP \-
initialize a terminal or query \fI\%term\%info\fP database
.SH SYNOPSIS
\fB@TPUT@\fP [\fB\-T\fP \fIterminal-type\fP]
{\fIcap-code\fP [\fIparameter\fP .\|.\|.\&]} .\|.\|.
.PP
\fB@TPUT@\fP [\fB\-T\fP \fIterminal-type\fP] [\fB\-x\fP] \fBclear\fP
.PP
\fB@TPUT@\fP [\fB\-T\fP \fIterminal-type\fP] \fBinit\fP
.PP
\fB@TPUT@\fP [\fB\-T\fP \fIterminal-type\fP] \fBreset\fP
.PP
\fB@TPUT@\fP [\fB\-T\fP \fIterminal-type\fP] \fBlongname\fP
.PP
\fB@TPUT@ \-S\fP
.PP
\fB@TPUT@ \-V\fP
.SH DESCRIPTION
\fB@TPUT@\fP uses the \fI\%term\%info\fP library and database to make
the values of terminal-specific capabilities and information available
to the shell,
to initialize or reset the terminal,
or report the long name of the current
(or specified)
terminal type.
When retrieving capability values,
the result depends upon the capability's type.
.TP 9 \" "Boolean" + 2n
Boolean
\fB@TPUT@\fP sets its exit status to
.B 0
if the terminal possesses
.I cap-code,
and
.B 1
if it does not.
.TP
integer
\fB@TPUT@\fP writes
.IR cap-code 's
decimal value to the standard output stream if defined
.RB ( \-1
if it is not)
followed by a newline.
.TP
string
\fB@TPUT@\fP writes
.IR cap-code 's
value to the standard output stream if defined,
without a trailing newline.
.PP
Before using a value returned on the standard output,
the application should test \fB@TPUT@\fP's exit status
(for example,
using \fB$?\fP in \fIsh\fP(1))
to be sure it is \fB0\fP;
see sections \*(``EXIT STATUS\*('' and \*(``DIAGNOSTICS\*('' below.
For a complete list of
.I cap-codes,
see \fB\%terminfo\fP(5).
.SS Options
.TP
\fB\-S\fP
allows more than one capability per invocation of \fB@TPUT@\fP.
The capabilities must be passed to \fB@TPUT@\fP from the standard input
instead of from the command line
(see example).
Only one \fIcap-code\fP is allowed per line.
The \fB\-S\fP option changes the
meaning of the \fB0\fP and \fB1\fP Boolean and string exit statuses
(see section \*(``EXIT STATUS\*('' below).
.IP
Because some capabilities may use
\fIstring\fP parameters rather than \fInumbers\fP,
\fB@TPUT@\fP uses a table and the presence of parameters in its input
to decide whether to use \fBtparm\fP(3X),
and how to interpret the parameters.
.TP
\fB\-T\fItype\fR
indicates the \fItype\fP of terminal.
Normally this option is
unnecessary, because the default is taken from the environment
variable \fITERM\fP.
If \fB\-T\fP is specified, then the shell
variables \fILINES\fP and \fI\%COLUMNS\fP will also be ignored.
.TP
\fB\-V\fP
reports the version of \fI\%ncurses\fP which was used in this program,
and exits.
.TP
.B \-x
prevents \fB\%@TPUT@\fP from attempting to clear the scrollback buffer.
.SS Commands
A few commands (\fBinit\fP, \fBreset\fP and \fBlongname\fP) are
special; they are defined by the \fB@TPUT@\fP program.
The others are the names of \fIcapabilities\fP from the terminal database
(see \fB\%terminfo\fP(5) for a list).
Although \fBinit\fP and \fBreset\fP resemble capability names,
\fB@TPUT@\fP uses several capabilities to perform these special functions.
.TP
\fIcap-code\fP
indicates the capability from the terminal database.
.IP
If the capability is a string that takes parameters, the arguments
following the capability will be used as parameters for the string.
.IP
Most parameters are numbers.
Only a few terminal capabilities require string parameters;
\fB@TPUT@\fP uses a table to decide which to pass as strings.
Normally \fB@TPUT@\fP uses \fBtparm\fP(3X) to perform the substitution.
If no parameters are given for the capability,
\fB@TPUT@\fP writes the string without performing the substitution.
.TP
\fBinit\fP
If the terminal database is present and an entry for the user's
terminal exists (see \fB\-T\fItype\fR, above), the following will
occur:
.RS
.TP 5
(1)
first, \fB@TPUT@\fP retrieves the current terminal mode settings
for your terminal.
It does this by successively testing
.RS
.bP
the standard error,
.bP
standard output,
.bP
standard input and
.bP
ultimately \*(``/dev/tty\*(''
.RE
.IP
to obtain terminal settings.
Having retrieved these settings, \fB@TPUT@\fP remembers which
file descriptor to use when updating settings.
.TP
(2)
if the window size cannot be obtained from the operating system,
but the terminal description
(or environment,
e.g.,
\fILINES\fP and \fI\%COLUMNS\fP variables specify this),
update the operating system's notion of the window size.
.TP
(3)
the terminal modes will be updated:
.RS
.bP
any delays (e.g., newline) specified in the entry will
be set in the tty driver,
.bP
tabs expansion will be turned on or off according to
the specification in the entry, and
.bP
if tabs are not expanded,
standard tabs will be set (every 8 spaces).
.RE
.TP
(4)
if present, the terminal's initialization strings will be
output as detailed in the \fB\%terminfo\fP(5) section on
.IR "Tabs and Initialization" ,
.TP
(5)
output is flushed.
.RE
.IP
If an entry does not
contain the information needed for any of these activities,
that activity will silently be skipped.
.TP
\fBreset\fP
This is similar to \fBinit\fP, with two differences:
.RS
.TP 5
(1)
before any other initialization,
the terminal modes will be reset to a \*(``sane\*('' state:
.RS
.bP
set cooked and echo modes,
.bP
turn off cbreak and raw modes,
.bP
turn on newline translation and
.bP
reset any unset special characters to their default values
.RE
.TP 5
(2)
Instead of putting out \fIinitialization\fP strings, the terminal's
\fIreset\fP strings will be output if present
(\fBrs1\fP, \fBrs2\fP, \fBrs3\fP, \fBrf\fP).
If the \fIreset\fP strings are not present, but \fIinitialization\fP
strings are, the \fIinitialization\fP strings will be output.
.RE
.IP
Otherwise, \fBreset\fP acts identically to \fBinit\fP.
.TP
.B longname
A terminfo entry begins with one or more names by which an
application can refer to the entry,
before the list of terminal capabilities.
The names are separated by \*(``|\*('' characters.
X/Open states that the last name is the \*(``long name\*(''
and also that it may include blanks.
.IP
\fB\%@TIC@\fP warns if the last name does not include blanks,
to accommodate old terminfo entries which treated
the long name as an optional feature.
The long name is often referred to as the description field.
.IP
If the terminal database is present and an entry for the user's terminal
exists
(see
.B \-T
.I type
above),
\fB\%@TPUT@\fP reports the terminal's description
(or \*(``long name\*('')
to the standard output,
without a trailing newline.
See \fB\%term\%info\fP(5).
.SS Aliases
\fB@TPUT@\fP handles the \fBclear\fP, \fBinit\fP and \fBreset\fP
commands specially:
it allows for the possibility that it is invoked by a link with those names.
.PP
If \fB@TPUT@\fP is invoked by a link named \fBreset\fP, this has the
same effect as \fB@TPUT@ reset\fP.
The \fB@TSET@\fP(1) utility also treats a link named \fBreset\fP specially.
.PP
Before \fI\%ncurses\fP 6.1,
the two utilities were different from each other:
.bP
\fB@TSET@\fP utility reset the terminal modes and special characters
(not done with \fB@TPUT@\fP).
.bP
On the other hand, \fB@TSET@\fP's repertoire of terminal capabilities for
resetting the terminal was more limited,
i.e., only \fBreset_1string\fP, \fBreset_2string\fP and \fBreset_file\fP
in contrast to the tab-stops and margins which are set by this utility.
.bP
The \fBreset\fP program is usually an alias for \fB@TSET@\fP,
because of this difference with resetting terminal modes and special characters.
.PP
With the changes made for \fI\%ncurses\fP 6.1,
the \fIreset\fP feature of the two programs is (mostly) the same.
A few differences remain:
.bP
The \fB@TSET@\fP program waits one second when resetting,
in case it happens to be a hardware terminal.
.bP
The two programs write the terminal initialization strings
to different streams (i.e., the standard error for \fB@TSET@\fP and the
standard output for \fB@TPUT@\fP).
.IP
\fBNote:\fP although these programs write to different streams,
redirecting their output to a file will capture only part of their actions.
The changes to the terminal modes are not affected by redirecting the output.
.PP
If \fB@TPUT@\fP is invoked by a link named \fBinit\fP, this has the
same effect as \fB@TPUT@ init\fP.
Again, you are less likely to use that link because another program
named \fBinit\fP has a more well-established use.
.SS "Terminal Size"
Besides the special commands (e.g., \fBclear\fP),
@TPUT@ treats certain terminfo capabilities specially:
\fBlines\fP and \fBcols\fP.
@TPUT@ calls \fBsetupterm\fP(3X) to obtain the terminal size:
.bP
first, it gets the size from the terminal database
(which generally is not provided for terminal emulators
which do not have a fixed window size)
.bP
then it asks the operating system for the terminal's size
(which generally works, unless connecting via a serial line which
does not support \fINAWS\fP: negotiations about window size).
.bP
finally, it inspects the environment variables \fILINES\fP and
\fI\%COLUMNS\fP which may override the terminal size.
.PP
If the \fB\-T\fP option is given
@TPUT@ ignores the environment variables by calling \fBuse_tioctl(TRUE)\fP,
relying upon the operating system (or finally, the terminal database).
.SH EXIT STATUS
If the \fB\-S\fP option is used,
\fB@TPUT@\fP checks for errors from each line,
and if any errors are found, will set the exit status to 4 plus the
number of lines with errors.
If no errors are found, the exit status is \fB0\fP.
No indication of which line failed can be given so
exit status \fB1\fP will never appear.
Exit statuses \fB2\fP, \fB3\fP, and
\fB4\fP retain their usual interpretation.
If the \fB\-S\fP option is not used,
the exit status depends on the type of \fIcap-code\fP:
.RS 3
.TP
.I Boolean
a value of \fB0\fP is set for TRUE and \fB1\fP for FALSE.
.TP
.I string
a value of \fB0\fP is set if the
\fIcap-code\fP is defined for this terminal \fItype\fP (the value of
\fIcap-code\fP is returned on standard output);
a value of \fB1\fP is set if \fIcap-code\fP
is not defined for this terminal \fItype\fP
(nothing is written to standard output).
.TP
.I integer
a value of \fB0\fP is always set,
whether or not \fIcap-code\fP is defined for this terminal \fItype\fP.
To determine if \fIcap-code\fP is defined for this terminal \fItype\fP,
the user must test the value written to standard output.
A value of \fB\-1\fP
means that \fIcap-code\fP is not defined for this terminal \fItype\fP.
.TP
.I other
\fBreset\fP or \fBinit\fP may fail to find their respective files.
In that case, the exit status is set to 4 + \fBerrno\fP.
.RE
.PP
Any other exit status indicates an error;
see section \*(``DIAGNOSTICS\*('' below.
.SH DIAGNOSTICS
\fB@TPUT@\fP prints the following error messages and sets the
corresponding exit statuses.
.PP
.ne 15
.TS
l l.
exit status	error message
=
\fB0\fP	T{
(\fIcap-code\fP is a numeric variable that is not specified in the
\fB\%terminfo\fP(5) database for this terminal type, e.g.
\fB@TPUT@ \-T450 lines\fP and \fB@TPUT@ \-Thp2621 xmc\fP)
T}
\fB1\fP	no error message is printed, see the \fBEXIT STATUS\fP section.
\fB2\fP	usage error
\fB3\fP	unknown terminal \fItype\fP or no \fI\%term\%info\fP database
\fB4\fP	unknown \fI\%term\%info\fP capability \fIcap-code\fP
\fB>4\fP	error occurred in \-S
=
.TE
.SH FILES
.TP
.I @DATADIR@/tabset
tab stop initialization database
.TP
.I \*d
compiled terminal description database
.SH PORTABILITY
This implementation of \fBtput\fP differs from AT&T \fBtput\fP in
two important areas:
.bP
\fB@TPUT@\fP \fIcap-code\fP writes to the standard output.
That need not be a regular terminal.
However, the subcommands which manipulate terminal modes
may not use the standard output.
.IP
The AT&T implementation's \fBinit\fP and \fBreset\fP commands
use the BSD (4.1c) \fBtset\fP source, which manipulates terminal modes.
It successively tries standard output, standard error, standard input
before falling back to \*(``/dev/tty\*('' and finally just assumes
a 1200Bd terminal.
When updating terminal modes, it ignores errors.
.IP
Until changes made after \fI\%ncurses\fP 6.0,
\fB@TPUT@\fP did not modify terminal modes.
\fB@TPUT@\fP now uses a similar scheme,
using functions shared with \fB@TSET@\fP
(and ultimately based on the 4.4BSD \fBtset\fP).
If it is not able to open a terminal, e.g., when running in \fBcron\fP(1),
\fB@TPUT@\fP will return an error.
.bP
AT&T \fBtput\fP guesses the type of its \fIcap-code\fP operands by
seeing if all of the characters are numeric,
or not.
.IP
Most implementations which provide support for \fIcap-code\fP operands
use the \fBtparm\fP function to expand parameters in it.
That function expects a mixture of numeric and string parameters,
requiring \fB@TPUT@\fP to know which type to use.
.IP
This implementation uses a table to determine the parameter types for
the standard \fIcap-code\fP operands, and an internal library
function to analyze nonstandard \fIcap-code\fP operands.
.IP
Besides providing more reliable operation than AT&T's utility,
a portability problem is introduced by this analysis:
An OpenBSD developer adapted the internal library function from
\fI\%ncurses\fP to port NetBSD's termcap-based \fBtput\fP to terminfo.
That had been modified to interpret multiple commands on a line.
Portable applications should not rely upon this feature;
\fI\%ncurses\fP provides it to support applications written
specifically for OpenBSD.
.PP
This implementation (unlike others) can accept both \fItermcap\fP
and \fIterminfo\fP names for the \fIcap-code\fP feature,
if
\fItermcap\fP support is compiled in.
However, the predefined \fItermcap\fP and \fIterminfo\fP names have two
ambiguities in this case (and the \fIterminfo\fP name is assumed):
.bP
The \fItermcap\fP name \fBdl\fP corresponds to
the \fIterminfo\fP name \fBdl1\fP (delete one line).
.br
The \fIterminfo\fP name \fBdl\fP corresponds to
the \fItermcap\fP name \fBDL\fP (delete a given number of lines).
.bP
The \fItermcap\fP name \fBed\fP corresponds to
the \fIterminfo\fP name \fBrmdc\fP (end delete mode).
.br
The \fIterminfo\fP name \fBed\fP corresponds to
the \fItermcap\fP name \fBcd\fP (clear to end of screen).
.PP
The \fBlongname\fP and \fB\-S\fP options, and the parameter-substitution
features used in the \fBcup\fP example,
were not supported in
AT&T/USL
.I curses
before SVr4 (1989).
Later, 4.3BSD-Reno (1990) added support for \fBlongname\fP,
.\" longname was added in October 1989.
and NetBSD (1994) added support for the parameter-substitution features.
.PP
IEEE Std 1003.1/The Open Group  Base Specifications Issue 7 (POSIX.1-2008)
documents only the operands for \fBclear\fP, \fBinit\fP and \fBreset\fP.
There are a few interesting observations to make regarding that:
.bP
In this implementation,
\fBclear\fP is part of the \fIcap-code\fP support.
The others (\fBinit\fP and \fBlongname\fP) do not correspond to terminal
capabilities.
.bP
Other implementations of \fBtput\fP on
SVr4-based systems such as Solaris, IRIX64 and HP-UX
as well as others such as AIX and Tru64
provide support for \fIcap-code\fP operands.
.bP
A few platforms such as FreeBSD recognize termcap names rather
than terminfo capability names in their respective \fBtput\fP commands.
Since 2010, NetBSD's \fBtput\fP uses terminfo names.
Before that, it (like FreeBSD) recognized termcap names.
.IP
Beginning in 2021, FreeBSD uses the \fI\%ncurses\fP \fBtput\fP,
configured for both terminfo (tested first) and termcap (as a fallback).
.PP
Because (apparently) \fIall\fP of the certified Unix systems
support the full set of capability names, the reasoning for documenting
only a few may not be apparent.
.bP
X/Open Curses Issue 7 documents \fBtput\fP differently,
with \fIcap-code\fP and the other features used in this implementation.
.bP
That is, there are two standards for \fBtput\fP:
POSIX (a subset) and X/Open Curses (the full implementation).
POSIX documents a subset to avoid the complication of including X/Open Curses
and the terminal capabilities database.
.bP
While it is certainly possible to write a \fBtput\fP program
without using
.I curses,
no system with a
.I curses
implementation provides a \fBtput\fP utility that does not also supply
the \fIcap-code\fP feature.
.PP
X/Open Curses Issue 7 (2009) is the first version to document utilities.
However that part of X/Open Curses does not follow existing practice
(that is,
System\ V
.I curses
behavior).
.bP
It assigns exit status 4 to \*(``invalid operand\*('',
which may be the same as \fIunknown capability\fP.
For instance, the source code for Solaris' xcurses uses the term
\*(``invalid\*('' in this case.
.bP
It assigns exit status 255 to a numeric variable that is not specified in
the terminfo database.
That likely is a documentation error,
confusing the \fB\-1\fP written to the standard output for an absent
or cancelled numeric value versus an (unsigned) exit status.
.PP
The various Unix systems (AIX, HP-UX, Solaris) use the same exit statuses
as \fI\%ncurses\fP.
.PP
NetBSD curses documents different exit statuses which do not correspond
to either \fI\%ncurses\fP or X/Open.
.SH HISTORY
The \fBtput\fP command was begun by Bill Joy in 1980.
The initial version only cleared the screen.
.PP
AT&T System V provided a different \fBtput\fP command:
.bP
SVr2 provided a rudimentary \fBtput\fP
which checked the parameter against each
predefined capability and returned the corresponding value.
This version of \fBtput\fP did not use \fBtparm\fP(3X) for
the capabilities which are parameterized.
.bP
SVr3 replaced that, a year later, by a more extensive program
whose \fBinit\fP and \fBreset\fP subcommands
(more than half the program) were incorporated from
the \fBreset\fP feature of BSD \fBtset\fP written by Eric Allman.
.bP
SVr4 added color initialization using the \fBorig_colors\fP and
\fBorig_pair\fP capabilities in the \fBinit\fP subcommand.
.PP
Keith Bostic replaced the BSD \fBtput\fP command in 1989
with a new implementation
based on the AT&T System V program \fBtput\fP.
Like the AT&T program, Bostic's version
accepted some parameters named for \fIterminfo\fP capabilities
(\fBclear\fP, \fBinit\fP, \fBlongname\fP and \fBreset\fP).
However (because he had only \fItermcap\fP available),
it accepted \fItermcap\fP names for other capabilities.
Also, Bostic's BSD \fBtput\fP did not modify the terminal I/O modes
as the earlier BSD \fBtset\fP had done.
.PP
At the same time, Bostic added a shell script named \*(``clear\*('',
which used \fBtput\fP to clear the screen.
.PP
Both of these appeared in 4.4BSD,
becoming the \*(``modern\*('' BSD implementation of \fBtput\fP.
.PP
This implementation of \fBtput\fP began from a different source than
AT&T or BSD: Ross Ridge's \fImytinfo\fP package, published on
\fIcomp.sources.unix\fP in December 1992.
Ridge's program made more sophisticated use of the terminal capabilities
than the BSD program.
Eric Raymond used that \fBtput\fP program
(and other parts of \fImytinfo\fP) in \fI\%ncurses\fP in June 1995.
Using the portions dealing with terminal capabilities
almost without change,
Raymond made improvements to the way the command-line parameters
were handled.
.SH EXAMPLES
.TP 5
\fB@TPUT@ init\fP
Initialize the terminal according to the type of
terminal in the environment variable \fITERM\fP.
This command should be included in everyone's .profile after
the environment variable \fITERM\fP has been exported,
as illustrated on the \fBprofile\fP(5) manual page.
.TP 5
\fB@TPUT@ \-T5620 reset\fP
Reset an AT&T 5620 terminal, overriding the type of
terminal in the environment variable \fITERM\fP.
.TP 5
\fB@TPUT@ cup 0 0\fP
Send the sequence to move the cursor to row \fB0\fP, column \fB0\fP
(the upper left corner of the screen, usually known as the \*(``home\*(''
cursor position).
.TP 5
\fB@TPUT@ clear\fP
Echo the clear-screen sequence for the current terminal.
.TP 5
\fB@TPUT@ cols\fP
Print the number of columns for the current terminal.
.TP 5
\fB@TPUT@ \-T450 cols\fP
Print the number of columns for the 450 terminal.
.TP 5
\fBbold=\(ga@TPUT@ smso\(ga offbold=\(ga@TPUT@ rmso\(ga\fP
Set the shell variables \fBbold\fP, to begin stand-out mode
sequence, and \fBoffbold\fP, to end standout mode sequence,
for the current terminal.
This might be followed by a
prompt: \fBecho "${bold}Please type in your name: ${offbold}\ec"\fP
.TP 5
\fB@TPUT@ hc\fP
Set exit status to indicate if the current terminal is a hard copy terminal.
.TP 5
\fB@TPUT@ cup 23 4\fP
Send the sequence to move the cursor to row 23, column 4.
.TP 5
\fB@TPUT@ cup\fP
Send the terminfo string for cursor-movement, with no parameters substituted.
.TP 5
\fB@TPUT@ longname\fP
Print the long name from the \fI\%term\%info\fP database for the
type of terminal specified in the environment
variable \fITERM\fP.
.TP 5
\fB@TPUT@ \-S\fP
The \fB\-S\fP option can be profitably used with a shell
\*(``here document\*(''.
.IP
.EX
$ \fB@TPUT@ \-S <<!\fP
> \fBclear\fP
> \fBcup 10 10\fP
> \fBbold\fP
> \fB!\fP
.EE
.IP
We see \fB@TPUT@\fP processing several capabilities in one invocation.
It clears the screen,
moves the cursor to position
(10, 10)
and turns on bold
(extra bright)
mode.
.TP 5
.B @TPUT@ clear cup 10 10 bold
Perform the same actions as the foregoing
.RB \%\[lq] @TPUT@
.BR \-S \[rq]
example.
.SH SEE ALSO
\fB\%@CLEAR@\fP(1),
\fB\%stty\fP(1),
\fB\%@TABS@\fP(1),
\fB\%@TSET@\fP(1),
\fB\%curs_termcap\fP(3X),
\fB\%terminfo\fP(5)
