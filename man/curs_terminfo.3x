'\" t
.\"***************************************************************************
.\" Copyright 2018-2022,2023 Thomas E. Dickey                                *
.\" Copyright 1998-2016,2017 Free Software Foundation, Inc.                  *
.\"                                                                          *
.\" Permission is hereby granted, free of charge, to any person obtaining a  *
.\" copy of this software and associated documentation files (the            *
.\" "Software"), to deal in the Software without restriction, including      *
.\" without limitation the rights to use, copy, modify, merge, publish,      *
.\" distribute, distribute with modifications, sublicense, and/or sell       *
.\" copies of the Software, and to permit persons to whom the Software is    *
.\" furnished to do so, subject to the following conditions:                 *
.\"                                                                          *
.\" The above copyright notice and this permission notice shall be included  *
.\" in all copies or substantial portions of the Software.                   *
.\"                                                                          *
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS  *
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF               *
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   *
.\" IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   *
.\" DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR    *
.\" OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR    *
.\" THE USE OR OTHER DEALINGS IN THE SOFTWARE.                               *
.\"                                                                          *
.\" Except as contained in this notice, the name(s) of the above copyright   *
.\" holders shall not be used in advertising or otherwise to promote the     *
.\" sale, use or other dealings in this Software without prior written       *
.\" authorization.                                                           *
.\"***************************************************************************
.\"
.\" $Id: curs_terminfo.3x,v 1.125 2023/12/30 23:46:56 tom Exp $
.TH curs_terminfo 3X 2023-12-30 "ncurses 6.4" "Library calls"
.ie \n(.g \{\
.ds `` \(lq
.ds '' \(rq
.\}
.el \{\
.ie t .ds `` ``
.el   .ds `` ""
.ie t .ds '' ''
.el   .ds '' ""
.\}
.
.de bP
.ie n  .IP \(bu 4
.el    .IP \(bu 2
..
.
.SH NAME
\fB\%del_curterm\fP,
\fB\%mvcur\fP,
\fB\%putp\fP,
\fB\%restartterm\fP,
\fB\%set_curterm\fP,
\fB\%setupterm\fP,
\fB\%tigetflag\fP,
\fB\%tigetnum\fP,
\fB\%tigetstr\fP,
\fB\%tiparm\fP,
\fB\%tiparm_s\fP,
\fB\%tiscan_s\fP,
\fB\%tparm\fP,
\fB\%tputs\fP,
\fB\%vid_attr\fP,
\fB\%vid_puts\fP,
\fB\%vidattr\fP,
\fB\%vidputs\fP \-
\fIcurses\fR interfaces to \fI\%term\%info\fR database
.SH SYNOPSIS
.nf
\fB#include <curses.h>
\fB#include <term.h>
.PP
\fBTERMINAL *cur_term;
.PP
\fBconst char * const boolnames[];
\fBconst char * const boolcodes[];
\fBconst char * const boolfnames[];
\fBconst char * const numnames[];
\fBconst char * const numcodes[];
\fBconst char * const numfnames[];
\fBconst char * const strnames[];
\fBconst char * const strcodes[];
\fBconst char * const strfnames[];
.PP
\fBint setupterm(const char *\fIterm\fP, int \fIfiledes\fP, int *\fIerrret\fP);
\fBTERMINAL *set_curterm(TERMINAL *\fInterm\fP);
\fBint del_curterm(TERMINAL *\fIoterm\fP);
\fBint restartterm(const char *\fIterm\fP, int \fIfiledes\fP, int *\fIerrret\fP);
.PP
\fBchar *tparm(const char *\fIstr\fP, \fR.\|.\|.\fP);
	\fI/* or */
\fBchar *tparm(const char *\fIstr\fP, long \fIp1\fP \fR.\|.\|.\fP \fBlong\fP \fIp9\fP);
.PP
\fBint tputs(const char *\fIstr\fP, int \fIaffcnt\fP, int (*\fIputc\fP)(int));
\fBint putp(const char *\fIstr\fP);
.PP
\fBint vidputs(chtype \fIattrs\fP, int (*\fIputc\fP)(int));
\fBint vidattr(chtype \fIattrs\fP);
\fBint vid_puts(attr_t \fIattrs\fP, short \fIpair\fP, void *\fIopts\fP, int (*\fIputc\fP)(int));
\fBint vid_attr(attr_t \fIattrs\fP, short \fIpair\fP, void *\fIopts\fP);
.PP
\fBint mvcur(int \fIoldrow\fP, int \fIoldcol\fP, int \fInewrow\fP, int \fInewcol\fP);
.PP
\fBint tigetflag(const char *\fIcapname\fP);
\fBint tigetnum(const char *\fIcapname\fP);
\fBchar *tigetstr(const char *\fIcapname\fP);
.PP
\fBchar *tiparm(const char *\fIstr\fP, \fR.\|.\|.\fP);
.PP
\fI/* extensions */
\fBchar *tiparm_s(int \fIexpected\fP, int \fImask\fP, const char *\fIstr\fP, ...);
\fBint tiscan_s(int *\fIexpected\fP, int *\fImask\fP, const char *\fIstr\fP);
.PP
\fI/* deprecated */
\fBint setterm(const char *\fIterm\fP);
.fi
.SH DESCRIPTION
These low-level routines must be called by programs that have to deal
directly with the
.I \%term\%info
database to handle certain terminal capabilities,
such as programming function keys.
For all other functionality,
.I curses
routines are more suitable and their use is recommended.
.PP
None of these functions use
(or are aware of)
multibyte character strings such as UTF-8.
.bP
Capability names and codes use the POSIX portable character set.
.bP
Capability string values have no associated encoding;
they are strings of 8-bit characters.
.SS Initialization
Initially,
\fB\%setupterm\fP should be called.
The high-level
.I curses
functions \fB\%initscr\fP and \fB\%newterm\fP call \fB\%setupterm\fP to
initialize the low-level set of terminal-dependent variables
listed in \fB\%term_variables\fP(3X).
.PP
Applications can use the
terminal capabilities either directly
(via header definitions),
or by special functions.
The header files
.I \%curses.h
and
.I \%term.h
should be included
(in that order)
to get the definitions for these strings, numbers, and flags.
.PP
The
.I \%term\%info
variables
\fBlines\fP and \fBcolumns\fP are initialized by \fB\%setupterm\fP as
follows:
.bP
If \fB\%use_env(FALSE)\fP has been called,
values for \fBlines\fP and \fBcolumns\fP specified in
.I \%term\%info
are used.
.bP
Otherwise,
if the environment variables \fILINES\fP and \fI\%COLUMNS\fP exist,
their values are used.
If these environment variables do not
exist and the program is running in a window, the current window size
is used.
Otherwise, if the environment variables do not exist, the
values for \fBlines\fP and \fBcolumns\fP specified in the
.I \%term\%info
database are used.
.PP
Parameterized strings should be passed through \fBtparm\fP to instantiate them.
All
.I \%term\%info
strings
(including the output of \fBtparm\fP)
should be printed
with \fBtputs\fP or \fBputp\fP.
Call \fBreset_shell_mode\fP to restore the
tty modes before exiting [see \fBcurs_kernel\fP(3X)].
.PP
Programs which use
cursor addressing should
.bP
output \fBenter_ca_mode\fP upon startup and
.bP
output \fBexit_ca_mode\fP before exiting.
.PP
Programs which execute shell subprocesses should
.bP
call \fBreset_shell_mode\fP and
output \fBexit_ca_mode\fP before the shell
is called and
.bP
output \fBenter_ca_mode\fP and
call \fBreset_prog_mode\fP after returning from the shell.
.PP
The \fB\%setupterm\fP routine reads in the
.I \%term\%info
database,
initializing the
.I \%term\%info
structures,
but does not set up the output virtualization structures used by
.I curses.
These are its parameters:
.RS 3
.TP 5
\fIterm\fP
is the terminal type, a character string.
If \fIterm\fP is null,
the environment variable \fITERM\fP is used.
.TP 5
\fIfiledes\fP
is the file descriptor used for getting and setting terminal I/O modes.
.IP
Higher-level applications use \fBnewterm\fP(3X) for initializing the terminal,
passing an output \fIstream\fP rather than a \fIdescriptor\fP.
In curses, the two are the same because \fBnewterm\fP calls \fBsetupterm\fP,
passing the file descriptor derived from its output stream parameter.
.TP 5
\fIerrret\fP
points to an optional location where an error status can be returned to
the caller.
If \fIerrret\fP is not null,
then \fBsetupterm\fP returns \fBOK\fP or
\fBERR\fP and stores a status value in the integer pointed to by
\fIerrret\fP.
A return value of \fBOK\fP combined with status of \fB1\fP in \fIerrret\fP
is normal.
.IP
If \fBERR\fP is returned, examine \fIerrret\fP:
.RS
.TP 5
.B 1
means that the terminal is hardcopy, cannot be used
for \fIcurses\fP applications.
.IP
\fB\%setupterm\fP determines if the entry is a hardcopy type by
checking the \fBhc\fP (\fBhardcopy\fP) capability.
.TP 5
.B 0
means that the terminal could not be found,
or that it is a generic type,
having too little information for \fIcurses\fP applications to run.
.IP
\fB\%setupterm\fP determines if the entry is a generic type by
checking the \fBgn\fP \%(\fBgeneric_type\fP) capability.
.TP 5
.B \-1
means that the
.I \%term\%info
database could not be found.
.RE
.IP
If \fIerrret\fP is
null, \fB\%setupterm\fP prints an error message upon finding an error
and exits.
Thus, the simplest call is:
.RS
.IP
\fBsetupterm((char *)0, 1, (int *)0);\fP
.RE
.IP
which uses all the defaults and sends the output to \fBstdout\fP.
.RE
.\" ***************************************************************************
.SS "The Terminal State"
The \fBsetupterm\fP routine stores its information about the terminal
in a \fI\%TERMINAL\fP structure pointed to by the global variable \fBcur_term\fP.
If it detects an error,
or decides that the terminal is unsuitable (hardcopy or generic),
it discards this information,
making it not available to applications.
.PP
If \fBsetupterm\fP is called repeatedly for the same terminal type,
it will reuse the information.
It maintains only one copy of a given terminal's capabilities in memory.
If it is called for different terminal types,
\fBsetupterm\fP allocates new storage for each set of terminal capabilities.
.PP
\fB\%set_curterm\fP sets \fB\%cur_term\fP to
.I \%nterm,
and makes all of the
.I \%term\%info
Boolean,
numeric,
and string variables use the values from
.I \%nterm.
It returns the old value of \fB\%cur_term\fP.
.PP
\fB\%del_curterm\fP routine frees the space pointed to by
.I \%oterm
and makes it available for further use.
If
.I \%oterm
is
the same as \fB\%cur_term\fP,
references to any of the
.I \%term\%info
Boolean,
numeric,
and string variables thereafter may refer to invalid memory locations
until another \fB\%setupterm\fP has been called.
.PP
The \fBrestartterm\fP routine is similar to \fBsetupterm\fP and \fBinitscr\fP,
except that it is called after restoring memory to a previous state (for
example, when reloading a game saved as a core image dump).
\fBrestartterm\fP assumes that the windows and the input and output options
are the same as when memory was saved,
but the terminal type and baud rate may be different.
Accordingly, \fBrestartterm\fP saves various tty state bits,
calls \fBsetupterm\fP, and then restores the bits.
.\" ***************************************************************************
.SS "Formatting Output"
The \fBtparm\fP routine instantiates the string \fIstr\fP with
parameters \fIpi\fP.  A pointer is returned to the result of \fIstr\fP
with the parameters applied.
Application developers should keep in mind these quirks of the interface:
.bP
Although \fBtparm\fP's actual parameters may be integers or strings,
the prototype expects \fBlong\fP (integer) values.
.bP
Aside from the \fBset_attributes\fP (\fBsgr\fP) capability,
most terminal capabilities require no more than one or two parameters.
.bP
Padding information is ignored by \fBtparm\fP;
it is interpreted by \fBtputs\fP.
.bP
The capability string is null-terminated.
Use \*(``\e200\*('' where an ASCII NUL is needed in the output.
.PP
\fB\%tiparm\fP is a newer form of \fB\%tparm\fP which uses
.I \%stdarg.h
rather than a fixed-parameter list.
Its numeric parameters are
.IR int s
rather than
.IR long s.
.PP
Both \fBtparm\fP and \fBtiparm\fP assume that the application passes
parameters consistent with the terminal description.
Two extensions are provided as alternatives to deal with untrusted data:
.bP
\fBtiparm_s\fP is an extension which is a safer formatting function
than \fBtparm\fR or \fBtiparm\fR,
because it allows the developer to tell the curses
library how many parameters to expect in the parameter list,
and which may be string parameters.
.IP
The \fImask\fP parameter has one bit set for each of the parameters
(up to 9) which will be passed as char* rather than numbers.
.bP
The extension \fBtiscan_s\fP allows the application
to inspect a formatting capability to see what the curses library would assume.
.\" ***************************************************************************
.SS "Output Functions"
String capabilities can contain padding information,
a time delay
(accommodating performance limitations of hardware terminals)
expressed as \fB$<\fIn\fB>\fR,
where \fIn\fP is a nonnegative integral count of milliseconds.
If \fIn\fP exceeds 30,000
(thirty seconds),
it is capped at that value.
.PP
The \fBtputs\fP routine interprets time-delay information in the string
\fIstr\fP and outputs it, executing the delays:
.bP
The \fIstr\fP parameter must be a terminfo string
variable or the return value from
\fBtparm\fP, \fBtiparm\fP, \fBtgetstr\fP, or \fBtgoto\fP.
.IP
The \fBtgetstr\fP and \fBtgoto\fP functions are part of the \fItermcap\fP
interface,
which happens to share this function name with the
.I \%term\%info
interface.
.bP
\fIaffcnt\fP is the number of lines affected, or 1 if
not applicable.
.bP
\fIputc\fP is a \fI\%putchar\fP-like function to which
the characters are passed, one at a time.
.IP
If \fBtputs\fP processes a time-delay,
it uses the \fBdelay_output\fP(3X) function,
routing any resulting padding characters through this function.
.PP
The \fBputp\fR routine calls \fBtputs(\fIstr\fB, 1, \%putchar)\fR.
The output of \fBputp\fP always goes to \fBstdout\fP, rather than
the \fIfiledes\fP specified in \fBsetupterm\fP.
.PP
The \fBvidputs\fP routine displays the string on the terminal in the
video attribute mode \fIattrs\fP, which is any combination of the
attributes listed in \fBcurses\fP(3X).
The characters are passed to
the \fI\%putchar\fP-like function \fIputc\fP.
.PP
The \fBvidattr\fP routine is like the \fBvidputs\fP routine, except
that it outputs through \fI\%putchar\fP.
.PP
.B \%vid_attr
and
.B \%vid_puts
correspond to
.B \%vidattr
and
.BR \%vidputs ,
respectively.
They use multiple parameters to represent the character attributes and
color;
namely,
.bP
.I \%attrs,
of type
.I \%attr_t,
for the attributes and
.bP
.I pair,
of type
.I short,
for the color pair number.
.PP
Use the attribute constants prefixed with
.RB \*(`` WA_ \*(''
with
.B \%vid_attr
and
.BR \%vid_puts .
.PP
X/Open Curses reserves the
.I opts
argument for future use,
saying that applications must provide a null pointer for that argument;
but see section \*(``EXTENSIONS\*('' below.
.PP
The \fBmvcur\fP routine provides low-level cursor motion.
It takes effect immediately (rather than at the next refresh).
Unlike the other low-level output functions,
which either write to the standard output or pass an output function parameter,
\fBmvcur\fP uses an output file descriptor derived from
the output stream parameter of \fBnewterm\fP(3X).
.PP
While \fBputp\fP and \fBmvcur\fP are low-level functions which
do not use the high-level curses state,
they are declared in
.I \%curses.h
because System\ V did this
(see \fIHISTORY\fP).
.\" ***************************************************************************
.SS "Terminal Capability Functions"
The \fBtigetflag\fP, \fBtigetnum\fP and \fBtigetstr\fP routines return
the value of the capability corresponding to the
.I \%term\%info
\fIcapname\fP passed to them, such as \fBxenl\fP.
The \fIcapname\fP for each capability is given in the table column entitled
\fIcapname\fP code in the capabilities section of \fB\%terminfo\fP(5).
.PP
These routines return special values to denote errors.
.PP
The \fBtigetflag\fP routine returns
.TP
\fB\-1\fP
if \fIcapname\fP is not a Boolean capability,
or
.TP
\fB0\fP
if it is canceled or absent from the terminal description.
.PP
The \fBtigetnum\fP routine returns
.TP
\fB\-2\fP
if \fIcapname\fP is not a numeric capability, or
.TP
\fB\-1\fP
if it is canceled or absent from the terminal description.
.PP
The \fBtigetstr\fP routine returns
.TP
\fB(char *)\-1\fP
if \fIcapname\fP is not a string capability,
or
.TP
\fB0\fP
if it is canceled or absent from the terminal description.
.\" ***************************************************************************
.SS "Terminal Capability Names"
These null-terminated arrays contain
.bP
the short \fI\%term\%info\fP names (\*(``codes\*(''),
.bP
the \fItermcap\fP names (\*(``names\*(''), and
.bP
the long \fI\%term\%info\fP names (\*(``fnames\*('')
.PP
for each of the predefined
.I \%term\%info
variables:
.PP
.RS
.nf
\fBconst char *boolnames[]\fP, \fB*boolcodes[]\fP, \fB*boolfnames[]\fP
\fBconst char *numnames[]\fP, \fB*numcodes[]\fP, \fB*numfnames[]\fP
\fBconst char *strnames[]\fP, \fB*strcodes[]\fP, \fB*strfnames[]\fP
.fi
.RE
.\" ***************************************************************************
.SS "Releasing Memory"
Each successful call to \fBsetupterm\fP allocates memory to hold the terminal
description.
As a side-effect, it sets \fBcur_term\fP to point to this memory.
If an application calls
.IP
\fBdel_curterm(cur_term);\fP
.PP
the memory will be freed.
.PP
The formatting functions \fBtparm\fP and \fBtiparm\fP extend the storage
allocated by \fBsetupterm\fP:
.bP
the \*(``static\*('' terminfo variables [a-z].
Before \fI\%ncurses\fP 6.3, those were shared by all screens.
With \fI\%ncurses\fP 6.3, those are allocated per screen.
See \fB\%terminfo\fP(5) for details.
.bP
to improve performance,
\fI\%ncurses\fP 6.3 caches the result of analyzing terminfo
strings for their parameter types.
That is stored as a binary tree referenced from the \fI\%TERMINAL\fP structure.
.PP
The higher-level \fBinitscr\fP and \fBnewterm\fP functions use \fBsetupterm\fP.
Normally they do not free this memory, but it is possible to do that using
the \fBdelscreen\fP(3X) function.
.\" ***************************************************************************
.SH RETURN VALUE
X/Open defines no failure conditions.
In
.I \%ncurses,
.TP 5
\fBdel_curterm\fP
returns an error
if its terminal parameter is null.
.TP 5
\fBputp\fP
calls \fBtputs\fP, returning the same error-codes.
.TP 5
\fBrestartterm\fP
returns an error
if the associated call to \fBsetupterm\fP returns an error.
.TP 5
\fBsetupterm\fP
returns an error
if it cannot allocate enough memory, or
create the initial windows
.RB ( \%stdscr ,
.BR \%curscr ,
and
.BR \%newscr )
Other error conditions are documented above.
.TP 5
\fBtparm\fP
returns a null if the capability would require unexpected parameters,
e.g., too many, too few, or incorrect types
(strings where integers are expected, or vice versa).
.TP 5
\fBtputs\fP
returns an error if the string parameter is null.
It does not detect I/O errors:
X/Open Curses states that \fBtputs\fP ignores the return value
of the output function \fIputc\fP.
.\" ***************************************************************************
.SH NOTES
The
.B \%vid_attr
function in
.I \%ncurses
is a special case.
It was originally implemented based on a draft of X/Open Curses,
as a macro,
before other parts of the
.I \%ncurses
wide-character API were developed,
and unlike the other wide-character functions,
is also provided in the non-wide-character configuration.
.\" ***************************************************************************
.SH EXTENSIONS
The functions marked as extensions were designed for
.I \%ncurses,
and are not found in SVr4
.IR curses ,
4.4BSD
.IR curses ,
or any other previous curses implementation.
.PP
.I \%ncurses
allows
.I opts
to be a pointer to
.I int,
which overrides the
.I pair
.RI ( short )
argument.
.\" ***************************************************************************
.SH PORTABILITY
\fB\%setterm\fP is not described by X/Open and must be considered
non-portable.
All other functions are as described by X/Open.
.SS "Compatibility Macros"
This implementation provides a few macros for compatibility with systems
before SVr4
(see section \*(``HISTORY\*('' below).
They include
\fB\%Bcrmode\fP,
\fB\%Bfixterm\fP,
\fB\%Bgettmode\fP,
\fB\%Bnocrmode\fP,
\fB\%Bresetterm\fP,
\fB\%Bsaveterm\fP, and
\fB\%Bsetterm\fP.
.PP
In SVr4,
these are found in
.IR \%curses.h ,
but except for \fB\%setterm\fP,
are likewise macros.
The one function,
\fB\%setterm\fP,
is mentioned in the manual page.
It further notes that \fB\%setterm\fP was replaced by \fB\%setupterm\fP,
stating that the call
.RS
.EX
setupterm(\fIterm\fP, 1, (int *)0)
.EE
.RE
provides the same functionality as \fB\%setterm(\fIterm\fB)\fR,
discouraging the latter for new programs.
.I \%ncurses
implements each of these symbols as macros for BSD
.I curses
compatibility.
.SS "Legacy Data"
\fB\%setupterm\fP copies the terminal name to the array \fB\%ttytype\fP.
This is not part of X/Open Curses, but is assumed by some applications.
.PP
Other implementions may not declare the capability name arrays.
Some provide them without declaring them.
X/Open does not specify them.
.PP
Extended terminal capability names,
as defined by
.RB \%\*(`` "@TIC@\ \-x" \*('',
are not stored in the arrays described here.
.SS "Output Buffering"
Older versions of \fI\%ncurses\fP assumed that the file descriptor
passed to \fB\%setupterm\fP from \fB\%initscr\fP or \fB\%newterm\fP uses
buffered I/O,
and would write to the corresponding stream.
In addition to the limitation that the terminal was left in
block-buffered mode on exit
(like System\ V
.IR curses ),
it was problematic because
.I \%ncurses
did not allow a reliable way to cleanup on receiving
.BR SIGTSTP .
.PP
The current version (ncurses6)
uses output buffers managed directly by
.I \%ncurses.
Some of the low-level functions described in this manual page write
to the standard output.
They are not signal-safe.
The high-level functions in
.I \%ncurses
employ alternate versions of these functions using the more reliable
buffering scheme.
.SS "Function Prototypes"
The X/Open Curses prototypes are based on the SVr4
.I curses
header declarations,
which were defined at the same time the C language was first
standardized in the late 1980s.
.bP
X/Open Curses uses
.I \%const
less effectively than a later design might,
in some cases applying it needlessly to values are already constant,
and in most cases overlooking parameters which normally would use
.I \%const.
Using constant parameters for functions which do not use
.I \%const
may prevent the program from compiling.
On the other hand,
\*(``writable strings\*('' are an obsolescent feature.
.IP
As an extension,
this implementation can be configured to change the function prototypes
to use the
.I \%const
keyword.
The
.I \%ncurses
ABI 6 enables this feature by default.
.bP
X/Open Curses prototypes \fB\%tparm\fP with a fixed number of
parameters,
rather than a variable argument list.
.IP
This implementation uses a variable argument list, but can be
configured to use the fixed-parameter list.
Portable applications should provide 9 parameters after the format;
zeroes are fine for this purpose.
.IP
In response to review comments by Thomas E. Dickey,
X/Open Curses Issue 7 proposed the \fB\%tiparm\fP function in mid-2009.
.IP
While \fB\%tiparm\fP is always provided in \fI\%ncurses\fP,
the older form is only available as a build-time configuration option.
If not specially configured,
\fB\%tparm\fP is the same as \fB\%tiparm\fP.
.PP
Both forms of \fB\%tparm\fP have drawbacks:
.bP
Most of the calls to \fB\%tparm\fP use only one or two parameters.
Passing nine on each call is awkward.
.IP
Using
.I long
for the numeric parameter type is a workaround to make the parameter use
the same amount of stack as a pointer.
That approach dates back to the mid-1980s, before C was standardized.
Since then, there is a standard
(and pointers are not required to fit in a long).
.bP
Providing the right number of parameters for a variadic function
such as \fB\%tiparm\fP can be a problem,
in particular for string parameters.
However, only a few terminfo capabilities use string parameters
(e.g., the ones used for programmable function keys).
.IP
The \fI\%ncurses\fP library checks usage of these capabilities,
and returns an error if the capability mishandles string parameters.
But it cannot check if a calling program provides strings in the right
places for the \fB\%tparm\fP calls.
.IP
The \fB\%@TPUT@\fR(1) program checks its use of these capabilities with
a table,
so that it calls \fB\%tparm\fP correctly.
.SS "Special \fITERM\fP treatment"
If configured to use the terminal-driver,
e.g., for the MinGW port,
.bP
\fB\%setupterm\fP interprets a missing/empty \fITERM\fP variable as the
special value \*(``unknown\*(''.
.IP
SVr4 curses uses the
special value \*(``dumb\*(''.
.IP
The difference between the two is that
the former uses the \fBgn\fP (\fB\%generic_type\fR) terminfo capability,
while the latter does not.
A generic terminal is unsuitable for full-screen applications.
.bP
\fB\%setupterm\fP allows explicit use of the
the windows console driver by checking if \fB$TERM\fP is set to
\*(``#win32con\*('' or an abbreviation of that string.
.SS "Other Portability Issues"
In SVr4,
\fB\%set_curterm\fP returns an
.I int,
.B OK
or
.BR ERR .
We have chosen to implement the X/Open Curses semantics.
.PP
In SVr4,
the third argument of \fB\%tputs\fP has the type
.RB \*(`` "int (*putc)(char)" \*(''.
.PP
At least one implementation of X/Open Curses (Solaris) returns a value
other than
.B OK
or
.B ERR
from \fB\%tputs\fP.
It instead returns the length of the string,
and does no error checking.
.PP
X/Open Curses notes that after calling \fB\%mvcur\fP,
the
.I curses
state may not match the actual terminal state,
and that an application should touch and refresh the window before
resuming normal
.I curses
calls.
Both
.I \%ncurses
and SVr4
.I curses
implement \fB\%mvcur\fP using the
.I SCREEN
data allocated in either \fB\%initscr\fP or \fB\%newterm\fP.
So though it is documented as a
.I \%term\%info
function,
\fB\%mvcur\fP is really a
.I curses
function that is not well specified.
.PP
X/Open notes that after calling \fBmvcur\fP, the curses state may not match the
actual terminal state, and that an application should touch and refresh
the window before resuming normal curses calls.
Both \fI\%ncurses\fP and System V Release 4 curses implement \fBmvcur\fP
using the \fISCREEN\fP data allocated in either \fBinitscr\fP or
\fBnewterm\fP.
So though it is documented as a terminfo function,
\fBmvcur\fP is really a curses function which is not well specified.
.PP
X/Open Curses states that the old location must be given for
\fB\%mvcur\fP to accommodate terminals that lack absolute cursor
positioning.
.\" X/Open Curses Issue 7, p. 161
.I \%ncurses
allows the caller to use \-1 for either or both old coordinates.
The \-1 tells
.I \%ncurses
that the old location is unknown,
and that it must use only absolute motion
(such as \fI\%cursor_address\fP)
rather than the least costly combination of absolute and relative motion.
.\" ***************************************************************************
.SH HISTORY
SVr2 (1984) introduced the
.I \%term\%info
feature.
Its programming manual mentioned the following low-level functions.
.PP
.TS
lB lB
lB lx.
Function	Description
_
fixterm	restore tty to \*(``in curses\*('' state
gettmode	establish current tty modes
mvcur	low level cursor motion
putp	use \fBtputs\fP to send characters via \fI\%putchar\fP
resetterm	set tty modes to \*(``out of curses\*('' state
resetty	reset tty flags to stored value
saveterm	save current modes as \*(``in curses\*('' state
savetty	store current tty flags
setterm	establish terminal with given type
setupterm	establish terminal with given type
tparm	interpolate parameters into string capability
tputs	apply padding information to a string
vidattr	like \fBvidputs\fP, but output through \fIputchar\fP
vidputs	write string to terminal, applying specified attributes
.TE
.PP
The programming manual also mentioned
functions provided for
.I termcap
compatibility
(commenting that they \*(``may go away at a later date\*('').
.PP
.TS
lB lB
lB lx.
Function	Description
_
tgetent	look up \fItermcap\fP entry for given \fIname\fP
tgetflag	get Boolean entry for given \fIid\fP
tgetnum	get numeric entry for given \fIid\fP
tgetstr	get string entry for given \fIid\fP
tgoto	apply parameters to given capability
tputs	write characters via a function parameter, applying padding
.TE
.PP
Early
.I \%term\%info
programs obtained capability values from the
.I \%TERMINAL
structure initialized by \fB\%setupterm\fP.
.PP
SVr3 (1987) extended
.I \%term\%info
by adding functions to retrieve capability values
(like the
.I termcap
interface),
and reusing \fB\%tgoto\fP and \fB\%tputs\fP.
.PP
.TS
lB lB
lB lx.
Function	Description
_
tigetflag	get Boolean entry for given \fIid\fP
tigetnum	get numeric entry for given \fIid\fP
tigetstr	get string entry for given \fIid\fP
.TE
.PP
SVr3 also replaced several of the SVr2
.I \%term\%info
functions that had no counterpart in the
.I termcap
interface,
documenting them as obsolete.
.PP
.TS
lB lB
l  lx.
Function	Replaced by
_
crmode	cbreak
fixterm	reset_prog_mode
gettmode	\fIn/a\fP
nocrmode	nocbreak
resetterm	reset_shell_mode
saveterm	def_prog_mode
setterm	setupterm
.TE
.PP
SVr3 kept the \fB\%mvcur\fP,
\fB\%vidattr\fP,
and \fB\%vidputs\fP functions,
along with \fB\%putp\fP,
\fB\%tparm\fP,
and \fB\%tputs\fP.
The latter were needed to support padding,
and to handle capabilities accessed by functions such as \fB\%vidattr\fP
(which used more than the two parameters supported by \fB\%tgoto\fP).
.PP
SVr3 introduced the functions for switching between terminal
descriptions;
for example,
\fB\%set_curterm\fP.
Some changes reflected incremental improvements to the SVr2 library.
.bP
The
.I \%TERMINAL
type definition was introduced in SVr3.01,
for the
.I term
structure provided in SVr2.
.bP
Various global variables such as \fB\%boolnames\fP were mentioned
in the programming manual at this point,
though the variables had been provided in SVr2.
.PP
SVr4 (1989) added the \fB\%vid_attr\fP and \fB\%vid_puts\fP functions.
.PP
Other low-level functions are declared in the
.I curses
header files of Unix systems,
but none are documented.
Those noted as \*(``obsolete\*('' by SVr3 remained in use by System\ V's
\fIvi\fP(1) editor.
.SH SEE ALSO
\fB\%curses\fP(3X),
\fB\%curs_initscr\fP(3X),
\fB\%curs_kernel\fP(3X),
\fB\%curs_memleaks\fP(3X),
\fB\%curs_termcap\fP(3X),
\fB\%curs_variables\fP(3X),
\fB\%putc\fP(3),
\fB\%term_variables\fP(3X),
\fB\%terminfo\fP(5)
